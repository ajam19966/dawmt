import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { Router, RouterOutlet } from '@angular/router';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./layout/header/header.component";
import { FooterComponent } from "./layout/footer/footer.component";
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { ProductComponent } from "./product/product.component";
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [AppComponent, HeaderComponent, FooterComponent,ProductComponent, SearchComponent],
  imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule,RouterOutlet,HttpClientModule,CommonModule,FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
