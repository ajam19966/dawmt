import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { MyProductInfo, ProductService } from "src/services/product.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent {
  cart: any;
  searchQuery: any;
  filteredProducts: MyProductInfo[] = [];
  constructor(private productService: ProductService,private router: Router) {}
  ngDoCheck(): void {
    this.cart = this.productService.getFromCart().length;
  }
  performSearch(): void {
    this.productService.setloader(true);
    this.productService.getProducts().subscribe((products) => {
      this.filteredProducts = products.filter(
        (product) =>
          product.title
            .toLowerCase()
            .includes(this.searchQuery.toLowerCase()) ||
          product.category
            .toLowerCase()
            .includes(this.searchQuery.toLowerCase())
      );
      this.productService.setProduct(this.filteredProducts);
      this.router.navigate(['/search']);
      this.productService.setloader(false);

    });
  }
}
