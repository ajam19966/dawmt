import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  
  { path: 'products', component: ProductComponent },
  { path: 'search', component: SearchComponent },
  // Other routes
  { path: '**', redirectTo: '/products', pathMatch: 'full' } // Redirect to home if no matching route
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
