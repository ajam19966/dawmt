import { Component, OnInit } from "@angular/core";
import { MyProductInfo, ProductService } from "src/services/product.service";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"],
})
export class SearchComponent {
  filteredProducts: MyProductInfo[] = [];
  loaderValue: any;

  constructor(private productService: ProductService) {}

  ngDoCheck(): void {
    this.filteredProducts = this.productService.getProduct();
    this.loaderValue = this.productService.getLoader();
  }
}
