import { Component, NgModule, OnInit } from "@angular/core";
import { ProductService } from "../../services/product.service";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.css"],
})
export class ProductComponent implements OnInit {
  products: any[] = [];
  productInfo!: any;
  loaderValue:any
  constructor(private productService: ProductService) {
    this.loaderValue=this.productService.getLoader()
  }

  ngOnInit(): void {
    this.loadProducts();
  }
  ngDoCheck(): void {
    this.loaderValue=this.productService.getLoader()
  }
  setDetails(product: any) {
    this.productInfo = product;
  }
  loadProducts() {
    this.productService.getProducts().subscribe((res) => {
        this.productService.setloader(false);
      this.products = res;
    });
  }
  deleteFromCart(id: any) {
    this.productService.deleteProduct(id);
  }
  addToCart(id: any) {
    this.productService.setToCart(id);
  }
  isIdInArray(id: number): boolean {
    return this.productService
      .getFromCart()
      .some((product) => product.id === id);
  }
}
