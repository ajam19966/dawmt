import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
const baseUrl: string = "https://fakestoreapi.com/products";
export interface MyProductInfo {
  id: number;
  title: string;
  price: number;
  description: string;
  category: string;
  image: string;
  rating: object;
}

@Injectable({
  providedIn: "root",
})
export class ProductService {
  products: any[] = []; // Assuming products is an array of objects
  productsSearch: any[] = []; // Assuming products is an array of search
  loader!: boolean ;
  constructor(private http: HttpClient) {}

  // Method to get products
  getProducts(): Observable<MyProductInfo[]> {
     return this.http.get<any>(baseUrl);
  }
  // to Set array in cart
  setToCart(value: any): void {
    this.products.push(value);
  }
  // to get array from cart
  getFromCart(): any[] {
    return this.products;
  }
  // to Set array in seach
  setProduct(value: any): void {
    this.productsSearch = value;
  }
  // to get array from seach
  getProduct(): any[] {
    return this.productsSearch;
  }
    // get loader
  setloader(value: boolean) {
    this.loader = value;
  }
  // get loader
  getLoader() {
    return this.loader;
  }
  // delete sepacifec item if find from cart
  deleteProduct(id: number): void {
    const index = this.products.findIndex((product) => product.id === id);
    if (index !== -1) {
      this.products.splice(index, 1);
    }
  }
}
